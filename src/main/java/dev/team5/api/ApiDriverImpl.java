package dev.team5.api;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ApiDriverImpl implements ApiDriver{
    public List<ApiObject> requestFromApi(String request) throws UnirestException {

        HttpResponse<String> response = Unirest.get("https://google-search3.p.rapidapi.com/api/v1/search/" + request)
                .header("x-rapidapi-key", "b5a9532b82mshe453c836099464fp1eb271jsnf5ded5d45ed5")
                .header("x-rapidapi-host", "google-search3.p.rapidapi.com")
                .asString();

        String json = response.getBody();
        JSONObject object = new JSONObject(json);
        JSONArray arr = object.getJSONArray("results");
        List<ApiObject> results = new ArrayList<>();
        for(int i = 0; i < arr.length(); i++){
            String str = arr.getJSONObject(i).getString("title");
            String str2 = arr.getJSONObject(i).getString("link");
            String str3 = arr.getJSONObject(i).getString("description");
            ApiObject apiObject = new ApiObject(str2, str3, str);
            results.add(apiObject);
        }
        return results;
    }

    public static void main(String[] args) {
        ApiDriverImpl api = new ApiDriverImpl();
        try {
            System.out.println(api.requestFromApi("q=captain+morgan&num=3").toString());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}

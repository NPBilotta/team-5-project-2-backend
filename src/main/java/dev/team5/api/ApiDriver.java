package dev.team5.api;

import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.List;

public interface ApiDriver {
    public List<ApiObject> requestFromApi(String request) throws UnirestException;
}
